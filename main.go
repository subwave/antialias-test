package main

import (
	"image"
	_ "image/png"
	"log"
	"math"
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/quasilyte/ebitengine-graphics"
	"github.com/quasilyte/gmath"
)

const (
	WIDTH  = 1920
	HEIGHT = 1080
)

var (
	Scales   = [4]float64{0.5, 1, 2, 4}
	Rotation = math.Pi / 6

	HShift = float64(WIDTH / (len(Scales) + 1))
	VShift = float64(HEIGHT / 3)

	Image  *ebiten.Image
	Sprite *graphics.Sprite
)

type Game struct{}

func main() {
	Image = readImage()
	Sprite = graphics.NewSprite()
	Sprite.SetImage(Image)
	Sprite.Pos.Base = &gmath.Vec{}

	rad := gmath.Rad(Rotation)
	Sprite.Rotation = &rad

	game := &Game{}
	ebiten.SetWindowSize(WIDTH, HEIGHT)
	if err := ebiten.RunGame(game); err != nil {
		log.Fatal(err)
	}
}

func (*Game) Draw(screen *ebiten.Image) {
	drawImageOptions := ebiten.DrawImageOptions{}
	drawImageOptions.Filter = ebiten.FilterLinear

	Sprite.Pos.Base.Y = VShift * 2

	for idx, scale := range Scales {
		// Draw directly using textures

		drawImageOptions.GeoM.Translate(-float64(Image.Bounds().Dx())/2, -float64(Image.Bounds().Dy())/2) // center the image
		drawImageOptions.GeoM.Rotate(Rotation)
		drawImageOptions.GeoM.Scale(scale, scale)
		drawImageOptions.GeoM.Translate(HShift*float64(idx+1), VShift)

		screen.DrawImage(Image, &drawImageOptions)

		drawImageOptions.GeoM.Reset()

		// Draw using sprites

		Sprite.Pos.Base.X = HShift * float64(idx+1)
		Sprite.SetScaleX(scale)
		Sprite.SetScaleY(scale)
		Sprite.Draw(screen)
	}
}

func (*Game) Update() error {
	return nil
}

func (*Game) Layout(outerWidth, outerHeight int) (int, int) {
	return WIDTH, HEIGHT
}

func readImage() *ebiten.Image {
	file, err := os.Open("player.png")
	if err != nil {
		log.Fatal(err)
	}

	decodedImage, _, err := image.Decode(file)
	if err != nil {
		log.Fatal(err)
	}

	return ebiten.NewImageFromImage(decodedImage)
}
